
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:56PM

See merge request itentialopensource/adapters/adapter-cisco_software_manager!15

---

## 0.4.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cisco_software_manager!13

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:15PM

See merge request itentialopensource/adapters/adapter-cisco_software_manager!12

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:50PM

See merge request itentialopensource/adapters/adapter-cisco_software_manager!11

---

## 0.4.0 [05-08-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/devops-netops/adapter-cisco_software_manager!10

---

## 0.3.4 [03-26-2024]

* Changes made at 2024.03.26_14:24PM

See merge request itentialopensource/adapters/devops-netops/adapter-cisco_software_manager!9

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_11:13AM

See merge request itentialopensource/adapters/devops-netops/adapter-cisco_software_manager!8

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_13:22PM

See merge request itentialopensource/adapters/devops-netops/adapter-cisco_software_manager!7

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:47PM

See merge request itentialopensource/adapters/devops-netops/adapter-cisco_software_manager!6

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-cisco_software_manager!5

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-cisco_software_manager!4

---

## 0.1.4 [11-08-2021]

- Adding special content-type for get calls determined in use with customer - changed all GET calls.

See merge request itentialopensource/adapters/devops-netops/adapter-cisco_software_manager!3

---

## 0.1.3 [03-03-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/devops-netops/adapter-cisco_software_manager!2

---

## 0.1.2 [07-07-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/devops-netops/adapter-cisco_software_manager!1

---

## 0.1.1 [05-09-2020]

- Initial Commit

See commit 0b0b2b4

---
