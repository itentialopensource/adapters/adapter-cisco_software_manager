## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cisco Software Manager. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cisco Software Manager.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Cisco Software Manager. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getGetCatalogClass(callback)</td>
    <td style="padding:15px">Returns the platforms and releases that are displayed under the CCO menu. The platform and release
information can be used to retrieve software information using other CCO APIs.

    Request Example:
    http://localhost:5000/api/v2/cco/catalog

    Response Example:
    {
        "data": {
            "asr9k_px": [
                "6.6.2",
                "6.6.1",
                "6.5.3",
                "6.5.2",
                "6.5.1",
                "6.4.2",
                "6.4.1"
      ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/cco/catalog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetOptimizedSoftwareClass(payload, callback)</td>
    <td style="padding:15px">Given a software package list, returns a list of software packages that should be included for a successful installation.
This API requires a connection to CCO to resolve software package dependencies.  If CCO is not reachable and
the dependency information was not previously saved in the database, the software packages submitted may be
classified as 'Unrecognized'.  The JSON tag, 'is',  in the returned response represents the classification of
the software packages.  See the table below.


  ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/cco/get_optimized_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGetSoftwareClass(release, platform, date, optimal = 'true', callback)</td>
    <td style="padding:15px">Returns all software information for a particular release and platform since a particular date (i.e. CCO posted date).
There are two types of software information namely SMU and Sevice Pack.

    Request Examples:
    http://localhost:5000/api/v2/cco/software/platform/asr9k_px/release/5.3.3
    http://localhost:5000/api/v2/cco/software/platform/asr9k_px/release/5.3.3?date=12-20-2015
    http://localhost:5000/api/v2/cco/software/platform/asr9k_px/release/5.3.3?date=12-20-2015&optimal=false

   ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/cco/software/platform/{pathv1}/release/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGetCcoSoftwareEntryClass(release, platform, nameOrId, callback)</td>
    <td style="padding:15px">Returns information related to a specific software item by its name or ID (e.g. a SMU/Sevice Pack/Release Software)

    Request Example:
    http://localhost:5000/api/v2/cco/software/platform/asr9k_px/release/5.3.3/name_or_id/AA14563

    Response Example:
    {
        "data": {
            "status": "Posted",
            "impact": "Needs Reboot",
            "compressed_image_size": "93065758",
            "posted_date": "08/14/2018 13:53:03 PDT",
            "name": "asr9k-px-5.3.3.CSCvj22...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/cco/software/platform/{pathv1}/release/{pathv2}/name_or_id/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManageCustomCommandProfileClass(profileName, callback)</td>
    <td style="padding:15px">Returns custom command profiles.

    Request Examples:
    http://localhost:5000/api/v2/custom_command_profiles
    http://localhost:5000/api/v2/custom_command_profiles?profile_name=NCS5500 Cmd Profile

    Response Example:
    {
        "data": {
            "custom_command_profile_list": [
                {
                    "profile_id": 5,
                    "profile_name": "NCS5500 Cmd Profile",
                    "created_by": "root",
                    "command_list": [
         ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/custom_command_profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postManageCustomCommandProfileClass(payload, callback)</td>
    <td style="padding:15px">Creates custom command profiles on CSM Server. If an existing profile is specified, it will cause an update
in the CSM database.

    Request Example:
    {
        "profile_name": "NCS5500 Cmd Profile",
        "command_list": [
            "show install inactive summary",
            "show ipv4 brief",
            "show version"
        ]
    }

    Response Example:
    {
        "data": {
            "custom_command_profile": {
                "profile_id": 5,
                "profile_name...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/custom_command_profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeleteCustomCommandProfileClass(profileName, callback)</td>
    <td style="padding:15px">Deletes a custom command profile on CSM Server.

    Request Example:
    http://localhost:5000/api/v2/custom_command_profiles/XR

    Response Example:
    {
        "data": {
            "profile_id": 5,
            "profile_name": "NCS5500 Cmd Profile"
        }
    }

    Error Example:
    {
        "errors": {
            "message": "Custom Command Profile 'NCS5500 Cmd Profile' does not exist."
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/custom_command_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGetHostPlatformAndVersionCountsClass(callback)</td>
    <td style="padding:15px">Return the number of devices managed by CSM Server by platform and by version.

    Response Example:
    {
        "data": {
            "total_hosts": 16,
            "counts": [
                {
                    "count": 1,
                    "software_version": "03.18.00.S",
                    "software_platform": "ASR900-XE"
                },
                {
                    "count": 2,
                    "software_version": "6.2.2",
                    "software_platform": "...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/executive_dashboard/get_host_platform_and_version_counts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGetMonthlyHostEncrollmentCountsClass(callback)</td>
    <td style="padding:15px">Return the number of hosts created on CSM Server by year and by month.

    Response Example:
    {
        "data": {
            "total_hosts": 9,
            "counts": [
                {
                    "count": 2,
                    "created_year": 2018,
                    "created_month": 12
                },
                {
                    "count": 1,
                    "created_year": 2019,
                    "created_month": 1
                },
                {
       ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/executive_dashboard/get_monthly_host_enrollment_counts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGetMontlyInstallationCountsClass(callback)</td>
    <td style="padding:15px">Return the number of various types of successful actions performed on the managed devices by year and by month.

    Response Example:
    {
        "data": {
            "total_counts": [
                {
                    "created_year": 2019,
                    "created_month": 2,
                    "total_monthly_installations": 1,
                    "install_action_list": [
                        "Pre-Check",
                        "Add",
                        "Activate",
      ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/executive_dashboard/get_monthly_installation_counts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGetMontlyInstallationCountsClassByHost(hostname, callback)</td>
    <td style="padding:15px">Return the number of various types of successful actions performed on a specific managed device by year and by month.

    Request Example:
    http://localhost:5000/api/v2/executive_dashboard/get_monthly_installation_counts/hostname/172.27.148.159

    Response Example:
    {
        "data": {
            "total_counts": [
                {
                    "created_year": 2019,
                    "created_month": 2,
                    "total_monthly_installations": 1,
                  ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/executive_dashboard/get_monthly_installation_counts/hostname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGetMonthlyUserEncrollmentCountsClass(callback)</td>
    <td style="padding:15px">Return the number of users created on CSM Server by year and by month.

    Response Example:
    {
        "data": {
            "counts": [
                {
                    "count": 1,
                    "created_year": 2018,
                    "created_month": 12
                },
                {
                    "count": 2,
                    "created_year": 2019,
                    "created_month": 3
                }
            ],
            "total_users": 3
        }
  ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/executive_dashboard/get_monthly_user_enrollment_counts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCreateHostClass(family = '8000', hostname, region, page, itemPerPage, callback)</td>
    <td style="padding:15px">By default, if item_per_page is not specified, CSM will produce 1000 records per page.  If there are more
records to display, the total_pages in the JSON response will be greater than 1.  Use the page parameter
to indicate which page to display.

    Request Examples:
    http://localhost:5000/api/v2/hosts
    http://localhost:5000/api/v2/hosts?hostname=MyNCS5500
    http://localhost:5000/api/v2/hosts?region=SJ Labs
    http://localhost:5000/api/v2/hosts?region=SJ Labs&page=2
    http://localh...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateHostClass(payload, callback)</td>
    <td style="padding:15px">Up to 4 connection methods can be provided to connect to a device.  They are

- vty A (vty_a_connection_protocol, vty_a_ip, vty_a_port)
- vty B (vty_b_connection_protocol, vty_b_ip, vty_b_port)
- console connection A (console_connection_protocol, console_a_ip, console_a_port)
- console connection B (console_connection_protocol, console_b_ip, console_b_port)

At least one connection method must be provided for a successful connection to the device.

CSM provides a default set of prompt strings ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeleteHostClass(hostname, callback)</td>
    <td style="padding:15px">Response Example:
    {
        "data": {
            "host_id": 4,
            "hostname": "NCS5500"
        }
    }

    Error Example:
    {
        "errors": {
            "message": "Host 'NCS5500' does not exist."
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/hosts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGetOrDeleteInstallJobClass(id, hostname, status = 'scheduled', callback)</td>
    <td style="padding:15px">If an install job ID (i.e. id) is specified in the request parameters, all other parameters will be ignored.
Install jobs (i.e. dependencies) which depend on this install job will also be deleted.  If there were
deleted dependencies, a JSON tag called deleted_dependencies with the deleted dependency install job IDs
will be added to the response entry.

For example,

"deleted_dependencies": [
    306,
    307
]

Each response entry in the install_job_list array contains a delete_status (success...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/install/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGetOrDeleteInstallJobClass(id, hostname, status = 'scheduled', installAction = 'Pre-Check', scheduledTime, callback)</td>
    <td style="padding:15px">Returns install jobs specified by the id, hostname, install_action, status, or jobs that have
scheduled times later than or equal to the submitted scheduled_time. Multiple criteria can be used at the
same time. If the submitted request would return more than 5000 entries, an error message will be shown asking
the user to further refine the query until it result is fewer than 5000 entries.

    Request Examples:
    http://localhost:5000/api/v2/install
    http://localhost:5000/api/v2/install?i...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/install/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInstallActivateClass(payload, callback)</td>
    <td style="padding:15px">Request Examples:
    http://localhost:5000/api/v2/install/activate

    {
        "hostname": "172.27.148.159",
        "scheduled_time": 1567811018,
        "software_packages": [
            "asr9k-px-6.2.2.CSCvd56753-1.0.0"
        ]
    }

    Response Example:
    {
        "data": {
            "install_job_id": 264
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/install/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInstallAddClass(payload, callback)</td>
    <td style="padding:15px">Request Examples:
    http://localhost:5000/api/v2/install/add

    {
        "hostname": "172.27.148.159",
        "scheduled_time": 1567811018,
        "server_repository": "csm-sj-alpha.cisco.com",
        "server_directory": "asr9k/6.2.2",
        "software_packages": [
            "asr9k-px-6.2.2.CSCvd56753.tar",
            "asr9k-px-6.2.2.CSCvf11168.tar",
            "asr9k-px-6.2.2.CSCvf23489.tar"
        ]
    }

    Response Example:
    {
        "data": {
            "install_j...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/install/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInstallCommitClass(payload, callback)</td>
    <td style="padding:15px">Commit, Rollback, Remove All Inactive accept the same set of JSON tags.

    Request Examples:
    http://localhost:5000/api/v2/install/commit
    http://localhost:5000/api/v2/install/rollback
    http://localhost:5000/api/v2/install/remove_all_inactive

    {
        "hostname": "172.27.148.159"
        "scheduled_time": 1567811018
    }

    Response Example:
    {
        "data": {
            "install_job_id": 266
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/install/commit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInstallRemoveAndDeactivateClass(payload, callback)</td>
    <td style="padding:15px">Remove and Deactivate accept the same set of JSON tags.

    Request Examples:
    http://localhost:5000/api/v2/install/remove
    http://localhost:5000/api/v2/install/deactivate

    {
        "hostname": "172.27.148.159",
        "scheduled_time": 1567811018,
        "software_packages": [
            "asr9k-bng-px-6.2.2",
            "asr9k-doc-px-6.2.2",
            "asr9k-li-px-6.2.2"
        ]
    }

    Response Example:
    {
        "data": {
            "install_job_id": 267
        ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/install/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFpdUpgradeClass(payload, callback)</td>
    <td style="padding:15px">If neither fpd_location nor fpd_type is specified, the FPD Upgrade will apply to all locations and FPD types.

    Request Examples:
    http://localhost:5000/api/v2/install/fpd-upgrade

    {
        "hostname": "172.27.148.159",
        "scheduled_time": 1567811018,
        "fpd_location": "0/RP0",
        "fpd_type": "IOFPGA"
    }

    Response Example:
    {
        "data": {
            "install_job_id": 267
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/install/fpd-upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGetInstallLogClass(installJobId, callback)</td>
    <td style="padding:15px">The response returns a zip file which contains the install job's log files. An example in python for retrieving
and extracting that zip file is shown below.

    import requests
    import zipfile
    import io

    resp = requests.get("http://localhost:5000/api/v2/install/logs/13",auth=(token, ""))

    try:
        zip = zipfile.ZipFile(io.BytesIO(resp.content))
        zip.extractall(directory_path)
    except zipfile.BadZipfile:
        print "No session logs."


    The variable "director...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/install/logs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPreAndPostCheckClass(payload, callback)</td>
    <td style="padding:15px">Pre-Check and Post-Check accept the same set of JSON tags.

    Request Examples:
    http://localhost:5000/api/v2/install/pre-check
    http://localhost:5000/api/v2/install/post-check

    {
        "hostname": "172.27.148.159",
        "scheduled_time": 1567811018,
        "command_profile": [
            "NCS5500 Cmd Profile"
        ]
    }

    Response Example:
    {
        "data": {
            "install_job_id": 262
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/install/post-check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPreAndPostCheckClass1(payload, callback)</td>
    <td style="padding:15px">Pre-Check and Post-Check accept the same set of JSON tags.

    Request Examples:
    http://localhost:5000/api/v2/install/pre-check
    http://localhost:5000/api/v2/install/post-check

    {
        "hostname": "172.27.148.159",
        "scheduled_time": 1567811018,
        "command_profile": [
            "NCS5500 Cmd Profile"
        ]
    }

    Response Example:
    {
        "data": {
            "install_job_id": 262
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/install/pre-check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInstallRemoveAndDeactivateClass2(payload, callback)</td>
    <td style="padding:15px">Remove and Deactivate accept the same set of JSON tags.

    Request Examples:
    http://localhost:5000/api/v2/install/remove
    http://localhost:5000/api/v2/install/deactivate

    {
        "hostname": "172.27.148.159",
        "scheduled_time": 1567811018,
        "software_packages": [
            "asr9k-bng-px-6.2.2",
            "asr9k-doc-px-6.2.2",
            "asr9k-li-px-6.2.2"
        ]
    }

    Response Example:
    {
        "data": {
            "install_job_id": 267
        ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/install/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInstallCommitClass1(payload, callback)</td>
    <td style="padding:15px">Commit, Rollback, Remove All Inactive accept the same set of JSON tags.

    Request Examples:
    http://localhost:5000/api/v2/install/commit
    http://localhost:5000/api/v2/install/rollback
    http://localhost:5000/api/v2/install/remove_all_inactive

    {
        "hostname": "172.27.148.159"
        "scheduled_time": 1567811018
    }

    Response Example:
    {
        "data": {
            "install_job_id": 266
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/install/remove_all_inactive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInstallCommitClass2(payload, callback)</td>
    <td style="padding:15px">Commit, Rollback, Remove All Inactive accept the same set of JSON tags.

    Request Examples:
    http://localhost:5000/api/v2/install/commit
    http://localhost:5000/api/v2/install/rollback
    http://localhost:5000/api/v2/install/remove_all_inactive

    {
        "hostname": "172.27.148.159"
        "scheduled_time": 1567811018
    }

    Response Example:
    {
        "data": {
            "install_job_id": 266
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/install/rollback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChassisTypeCount(region, callback)</td>
    <td style="padding:15px">Return the list of chassis in the database (alternatively the specified region) in their associated quantities.

    Request Examples:
    http://127.0.0.1:5000/api/v2/inventory/get_chassis_type_count/
    http://127.0.0.1:5000/api/v2/inventory/get_chassis_type_count/?region=virtual-machine

    Response Example:
    {
      "data": {
        "counts": [
          {
            "chassis_type": "ASR-9010",
            "entity_count": 208
          },
          {
            "chassis_type": "NCS...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/inventory/get_chassis_type_count/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModelNameCount(region, callback)</td>
    <td style="padding:15px">Return the list of models in the database (alternatively the specified region) in their associated quantities.

    Request Examples:
    http://127.0.0.1:5000/api/v2/inventory/get_model_name_count/?region=virtual-machine

    Response Example:
    {
      "data": {
        "counts": [
          {
            "count": 300,
            "model_name": "A9K-RSP440-SE"
          },
          {
            "count": 300,
            "model_name": "ASR-9010-FAN"
          },
          {
            "c...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/inventory/get_model_name_count/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostnameInventory(hostname, callback)</td>
    <td style="padding:15px">Return the inventory of a single host specified by host name.

    Request Examples:
    http://127.0.0.1:5000/api/v2/inventory/hosts/ASR907

    Response Example:
    {
      "data": {
        "host_id": 1,
        "hostname": "ASR907",
        "inventory_list": [
          {
            "description": "ASR 907 Series Router Chassis",
            "hardware_revision": "V00",
            "location": "",
            "model_name": "ASR-907",
            "name": "Chassis",
            "serial_numb...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/inventory/hosts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInventorySearch(modelName, serialNumber, chassisType, callback)</td>
    <td style="padding:15px">Returns a list of hosts that contain a given model name, serial number, or chassis type.

    Request Examples:
    http://127.0.0.1:5000/api/v2/inventory/search/?model_name=A900-IMA8T
    http://127.0.0.1:5000/api/v2/inventory/search/?serial_number=AGA1135N3JB
    http://127.0.0.1:5000/api/v2/inventory/search/?chassis_type=ASR-907

    Response Example:
    {
      "data": {
        "host_count": 2,
        "host_list": [
          {
            "host_id": 1,
            "hostname": "ASR907",...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/inventory/search/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManageJumpHostClass(hostname, callback)</td>
    <td style="padding:15px">Response Example:
    "data": {
        "jump_host_list": [
            {
                "host_id": 3,
                "hostname": "sj20lab-as1",
                "host_or_ip": "10.34.23.345",
                "connection_protocol": "telnet",
                "username": "root",
                "username_prompt": "",
                "password_prompt": "",
                "port_number": 23,
                "device_prompt": ""
            },
            {
                "host_id": 2,
        ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/jump_hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postManageJumpHostClass(payload, callback)</td>
    <td style="padding:15px">device_prompt, username_prompt, and password_prompt are prompt strings or regular expressions that CSM
    uses to detect the various prompts returned by the jump host.  If not specified, CSM uses the
    default prompt string definitions (e.g. 'username' for username prompt, and 'password' for password prompt).

    Request Example:
    {
        "hostname": "sj20lab-as3",
        "host_or_ip": "172.23.45.124",
        "connection_protocol": "telnet",
        "username": "root",
        "...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/jump_hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeleteJumpHostClass(hostname, callback)</td>
    <td style="padding:15px">Response Example:
    {
        "data": {
            "host_id": 4,
            "hostname": "sj20lab-as3"
        }
    }

    Error Example:
    {
        "errors": {
            "message": "Jump host 'sj20lab-as3' does not exist."
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/jump_hosts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManageRegionClass(regionName, callback)</td>
    <td style="padding:15px">Response Example:
    {
        "data": {
            "region_list": [
                {
                    "server_repositories": [
                        "MyFTP",
                        "MySCP"
                    ],
                    "region_id": 3,
                    "region_name": "California USA"
                },
                {
                    "server_repositories": [],
                    "region_id": 2,
                    "region_name": "EMEAR XR"
                },...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/regions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postManageRegionClass(payload, callback)</td>
    <td style="padding:15px">The server repository specified in the request must have already been created.

    Request Example:
    {
        "region_name": "San Jose",
        "server_repositories": [
            "MyFTP"
        ]
    }

    Response Example:
    {
        "data": {
            "region": {
                "server_repositories": [
                    "MyFTP"
                ],
                "region_id": 6,
                "region_name": "San Jose"
            }
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/regions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeleteRegionClass(regionName, callback)</td>
    <td style="padding:15px">Response Example:
    {
        "data": {
            "region_id": 8,
            "region_name": "SJC-21"
        }
    }

    Error Example:
    {
        "errors": {
            "message": "Unable to delete region 'SJC-20'. Verify that it is not used by other hosts."
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGetServerRepositoryClass(serverRepositoryName, callback)</td>
    <td style="padding:15px">Different server repository types return different JSON output tags. See relevant server repository
    POST responses for more information.

    Response Example:
    {
        "data": {
            "server_repository_list": [
                {
                    "server_repository_id": 15,
                    "server_type": "FTP",
                    "server_repository_name": "MyFTP",
                    "server_address": "172.25.142.102",
                    "home_directory": "/tftpboo...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/server_repositories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateFtpClass(payload, callback)</td>
    <td style="padding:15px">Request Example:
    {
        "server_repository_name": "MyFTP",
        "server_address": "172.25.142.102",
        "username": "cisco",
        "password": "cisco",
        "home_directory": "/tftpboot/asr9k/6.3.3",
        "vrf": "Mgmt"
    }

    Response Example:
    {
        "data": {
            "server_repository": {
                "server_repository_id": 15,
                "server_type": "FTP",
                "server_repository_name": "MyFTP",
                "server_address"...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/server_repositories/server_types/ftp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateLocalClass(payload, callback)</td>
    <td style="padding:15px">Request Example:
    {
        "server_repository_name": "Router Local",
        "device_path": "harddisk0:"
    }

    Response Example:
    {
        "data": {
            "server_repository": {
                "server_repository_id": 17,
                "server_type": "LOCAL",
                "server_repository_name": "Router Local",
                "device_path": "harddisk0:"
            }
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/server_repositories/server_types/local?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateScpClass(payload, callback)</td>
    <td style="padding:15px">Request Example:
    {
        "server_repository_name": "MySCP",
        "server_address": "172.25.142.102",
        "home_directory": "/tftpboot/asr9k/6.3.3",
        "username": "cisco",
        "password": "cisco",
        "destination_on_host": "harddisk0:"
    }

    Response Example:
    {
        "data": {
            "server_repository": {
                "server_repository_id": 18,
                "server_type": "SCP",
                "server_repository_name": "MySCP",
          ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/server_repositories/server_types/scp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateSftpClass(payload, callback)</td>
    <td style="padding:15px">Request Example:
    {
        "server_repository_name": "MySFTP",
        "server_address": "172.25.142.102",
        "username": "cisco",
        "password": "cisco",
        "home_directory": "/tftpboot/asr9k/6.3.3"
    }

    Response Example:
    {
        "data": {
            "server_repository": {
                "server_repository_id": 15,
                "server_type": "SFTP",
                "server_repository_name": "MySFTP",
                "server_address": "172.25.142.102",
...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/server_repositories/server_types/sftp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateTftpClass(payload, callback)</td>
    <td style="padding:15px">Request Example:
    {
        "server_repository_name": "MyTFTP",
        "tftp_server_path": "tftp://223.255.254.253/auto/tftp-sjc-users3",
        "file_directory": "/auto/tftp-sjc-users3",
        "vrf": "Mgmt"
    }

    Response Example:
    {
        "data": {
            "server_repository": {
                "server_repository_id": 13,
                "server_type": "TFTP",
                "server_repository_name": "MyTFTP",
                "tftp_server_path": "tftp://223.255.254....(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/server_repositories/server_types/tftp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeleteServerRepositoryClass(serverRepositoryName, callback)</td>
    <td style="padding:15px">Response Example:
    {
        "data": {
            "server_repository_id": 16
            "server_repository_name": "MySFTP"
        }
    }

    Error Example:
    {
        "errors": {
            "message": "Server Repository 'MySFT' does not exist."
        }
    }</td>
    <td style="padding:15px">{base_path}/{version}/server_repositories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
