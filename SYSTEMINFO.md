# Cisco Software Manager

Vendor: Cisco
Homepage: https://www.cisco.com/

Product: Cisco Software Manager
Product Page: https://www.cisco.com/c/en/us/td/docs/routers/asr9000/software/smu/b-csm-install-guide/m-csm-overview.html

## Introduction
We classify Cisco Software Manager into the CI/CD domain as Cisco Software Manager facilitates the automation of software deployment process

## Why Integrate
The Cisco Software Manager adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco Software Manager.

With this adapter you have the ability to perform operations with Cisco Software Manager such as:

- Install
- Inventory
- Host

## Additional Product Documentation
The [API documents for Cisco Software Manager](https://community.cisco.com/t5/service-providers-knowledge-base/cisco-software-manager-api-guide/ta-p/3196126)